
## Config Python and Jupyter Notebook

### install Conda with python3.6

Conda can be  donwloaded from https://www.anaconda.com/products/individual

To create the conda environment nisar-biomass with python 3.6

**conda create -n nisar-biomass python=3.6 anaconda**

### install jupyter notebook (>3.0) with widgets
**conda install -c conda-forge jupyterlab=3**

try the **jupyter-lab --version**, if there are errors, run the command may fix the issue

**conda install -c conda-forge jupyter_client=7.1.0 jupyter_server=1.13.1  --force-reinstall**

finally, create a python environment for jupternotebook

**python -m ipykernel install --user --name=nisar**
